﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;


namespace UART_Logger
{
    public partial class FormUART_Logger : Form
    {
        private string ErrorLogFilePath;
        private StringBuilder csvcontent = new StringBuilder(); // No Initializazion needed
        private const int refresh_period = 100;

        public FormUART_Logger()
        {
            InitializeComponent();
        }

        private void FormUART_Logger_Load(object sender, EventArgs e)
        {

            if (serialPort1.IsOpen == false) 
            {
                buttonDisconnect.Enabled = false;
                string[] port = System.IO.Ports.SerialPort.GetPortNames(); 
                foreach (string item in port) 
                {
                    comboBoxPort.Items.Add(item); 
                }
                if (comboBoxPort.Items.Count == 0)
                {
                    MessageBox.Show("No available COMs");
                    this.Close();
                }
                else
                {
                    buttonConnect.Enabled = true; 
                }
            }

            LoadSettings();
        }


        private void buttonConnect_Click(object sender, EventArgs e)
        {
                try
                {
                    if (Create_ErrorLogFile() == false)
                    {
                        // Unable to create csv file
                        MessageBox.Show("Error: Unable to create csv file.");

                        return;
                    }


                if ((comboBoxPort.Text == "" ) || (comboBoxBaudrate.Text == ""))
                    {
                        MessageBox.Show("Please select port settings");
                    }
                    else
                    {
                        serialPort1.PortName = comboBoxPort.Text;
                        serialPort1.BaudRate = Convert.ToInt32(comboBoxBaudrate.Text);
                        serialPort1.Open();
                        progressBarConnect.Value = 100;
                        buttonConnect.Enabled = false;
                        buttonDisconnect.Enabled = true;
                        tabControl1.SelectedIndex = 1;
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("Unauthorized Access");
                }
            

        }


        private bool Create_ErrorLogFile()
        {
            saveFileDialog1.Filter = " csv files (*.csv)|*.csv";
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ErrorLogFilePath = saveFileDialog1.FileName;

                File.Delete(ErrorLogFilePath);

                return true;
            }
            else
            {
                return false;
            }

        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.Close();
                progressBarConnect.Value = 0;
                buttonDisconnect.Enabled = false;
                buttonConnect.Enabled = true;
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Unauthorized Access");
            }

        }


        private void LoadSettings()
        {
            comboBoxBaudrate.Text = Properties.Settings.Default.port;
            comboBoxPort.Text = Properties.Settings.Default.buadrate;
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.buadrate = comboBoxBaudrate.Text;
            Properties.Settings.Default.port = comboBoxPort.Text;
            Properties.Settings.Default.Save();
            }

         private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //richTextBox1 called by main thread
            richTextBox1.Invoke(new Action(delegate ()
            {
                string linData;
                linData = serialPort1.ReadExisting();
                richTextBox1.AppendText(linData);
                
                //save to csv every data
                try
                {
                    csvcontent.AppendLine(linData);
                    File.AppendAllText(ErrorLogFilePath, csvcontent.ToString());
                }
                catch(IOException)
                {
                    MessageBox.Show("Error! Close csv file and connect port again");
                }

            }));
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }


        private void FormLoggerFilter_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                try
                {
                    serialPort1.Close();
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
            SaveSettings();
        }
    }
}
